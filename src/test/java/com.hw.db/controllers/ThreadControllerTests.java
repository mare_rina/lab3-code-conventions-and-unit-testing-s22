package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ThreadControllerTests {

    @Test
    @DisplayName("Get thread by ID")
    public void testCheckId() {
        String idString = "777";
        int idInt = 777;

        ThreadController controller = new ThreadController();
        Thread thread = new Thread();
        thread.setId(idInt);

        try (MockedStatic<ThreadDAO> imposterThread = Mockito.mockStatic(ThreadDAO.class)) {
            imposterThread.when(() -> ThreadDAO.getThreadById(idInt)).thenReturn(thread);
            assertEquals(controller.CheckIdOrSlug(idString), thread);
        }
    }

    @Test
    @DisplayName("Get thread by slug")
    public void testCheckSlug() {
        String slug = "slug";

        ThreadController controller = new ThreadController();
        Thread thread = new Thread();
        thread.setSlug(slug);

        try (MockedStatic<ThreadDAO> imposterThread = Mockito.mockStatic(ThreadDAO.class)) {
            imposterThread.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(controller.CheckIdOrSlug(slug), thread);
        }
    }

    @Test
    @DisplayName("Create posts")
    public void testCreatePost() {
        ThreadController controller = new ThreadController();
        Thread thread = new Thread();
        String slug = "slug";
        thread.setSlug(slug);

        User user = new User();
        String username = "user";
        user.setNickname(username);

        Post post = new Post();
        post.setAuthor(username);
        List<Post> postList = new ArrayList<>();
        postList.add(post);

        try (MockedStatic<ThreadDAO> imposterThread = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> imposterUser = Mockito.mockStatic(UserDAO.class)) {
                imposterThread.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                imposterUser.when(() -> UserDAO.Info(username)).thenReturn(user);

                assertEquals(
                        ResponseEntity.status(HttpStatus.CREATED).body(postList),
                        controller.createPost(slug, postList));
            }
        }
    }

    @Test
    @DisplayName("Get posts")
    public void testPosts() {
        ThreadController controller = new ThreadController();
        Thread thread = new Thread();
        String slug = "slug";
        int id = 777;
        thread.setSlug(slug);
        thread.setId(id);

        Post post = new Post();
        List<Post> postList = new ArrayList<>();
        postList.add(post);
        post.setThread(id);

        try (MockedStatic<ThreadDAO> imposterThread = Mockito.mockStatic(ThreadDAO.class)) {
            imposterThread.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

            imposterThread.when(() -> ThreadDAO.getPosts(id, 1, 0, "flat", false)).thenReturn(postList);

            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(postList),
                    controller.Posts(slug, 1, 0, "flat", false));
        }

    }

    @Test
    @DisplayName("Change threads")
    public void testChange() {
        ThreadController controller = new ThreadController();
        Thread thread1 = new Thread("author1", Timestamp.valueOf("2001-11-11 11:11:11"), "forum1",
                "message1", "slug1", "title1", 1);
        int id1 = 1;
        thread1.setId(id1);

        Thread thread2 = new Thread("author2", Timestamp.valueOf("2002-12-22 22:22:22"), "forum2",
                "message2", "slug2", "title2", 2);
        int id2 = 2;
        thread2.setId(id2);

        try (MockedStatic<ThreadDAO> imposterThread = Mockito.mockStatic(ThreadDAO.class)) {
            imposterThread.when(() -> ThreadDAO.getThreadBySlug("slug1")).thenReturn(thread1);
            imposterThread.when(() -> ThreadDAO.getThreadById(id1)).thenReturn(thread1);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread1),
                    controller.change("slug1", thread2));
        }
    }

    @Test
    @DisplayName("Get info for thread")
    public void testInfo() {
        ThreadController controller = new ThreadController();
        Thread thread = new Thread();
        String slug = "slug";
        thread.setSlug(slug);

        try (MockedStatic<ThreadDAO> imposterThread = Mockito.mockStatic(ThreadDAO.class)) {
            imposterThread.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(thread),
                    controller.info(slug));
        }
    }

    @Test
    @DisplayName("Create vote")
    public void testCreateVote() {
        ThreadController controller = new ThreadController();
        Thread thread = new Thread();
        String slug = "slug";
        thread.setSlug(slug);
        thread.setVotes(1);

        User user = new User();
        String username = "user";
        user.setNickname(username);
        Vote vote = new Vote(username, 1);

        try (MockedStatic<ThreadDAO> imposterThread = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> imposterUser = Mockito.mockStatic(UserDAO.class)) {
                imposterUser.when(() -> UserDAO.Info(username)).thenReturn(user);
                imposterThread.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                        controller.createVote(slug, vote));
            }
        }
    }
}


